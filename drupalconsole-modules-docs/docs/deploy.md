## ¡Despliega!

Hasta ahora, tu sitio web sólo está disponible en tu computadora. ¡Ahora aprenderás como deployarlo! El deploy es el proceso de publicar tu web o aplicación en internet para que otras personas puedan acceder :)

Como ya has aprendido, un sitio web tiene que estar en un servidor. Hay muchos proveedores de servidores disponibles en internet, nosotras vamos a usar [Heroku](https://www.heroku.com){target=_blank}. <br/>
Heroku es gratuito para aplicaciones pequeñas que no tienen muchos visitantes, y con eso tendrás más que suficiente por ahora.

## Vas a necesitar una cuenta en Heroku
```
https://signup.heroku.com/login
```

## Instalación de Heroku CLI

Ahora vamos a instalar este paquete que nos ayudara a correr los comandos necesarios para crear nuestra app y deployar nuestro sitio en los servicios de Heroku.

<h5>MacOS</h5>
```
brew tap heroku/brew && brew install heroku
```
<h5>Windows</h5>
```
https://devcenter.heroku.com/articles/heroku-cli
Descargar el instalador
```
<h5>Linux</h5>
```
sudo snap install --classic heroku
```

## Primer paso: subir nuestro proyecto a un repositorio de Heroku

<h6>¿Que es un repositorio?</h6>
Un repositorio es un espacio centralizado donde se almacenan los archivos de tu proyecto.

En el directorio de nuestro proyecto ejecutamos:
```
git init
```

Una buena práctica antes de subir nuestros archivos al repositorio remoto es ignorar los archivos que pueden contener información confidencial o directorios de dependencias, ya que en estos se autogenerarán cuando instalamos nuestro drupal con Lando.

Crearemos el archivo .gitignore y le agregaremos lo siguiente:
```
.env
.lando.yml
vendor/

```

Ahora si podemos agregar todos los archivos de nuestro proyecto inicial con el siguiente comando:

```
git add .
```

Para ver todos los archivos que subiremos a nuestro repositorio remoto podremos consultarlo de la siguiente manera:

```
git status
```

Creamos nuestro primer commit para iniciar el proyecto.

```
git commit -m "Initial commit"
```
Entre las comillas podemos describir de qué se tratan los cambios que estamos confirmando. En este caso estamos haciendo un commit inicial.

<h5>¿Que es un commit?</h5>
Confirmar​ o hacer un commit se refiere a confirmar un conjunto de cambios provisionales de forma permanente. Con este commit tendremos la primer versión de nuestro proyecto.

Una vez creado nuestro commit podremos pushear nuestro proyecto para publicarlo en nuestro repositorio de Heroku.


<h5>Vamos a crear el proyecto en Heroku:</h5>
```
heroku create drupal-demo-ba
```
Recordá cambiar el nombre del sitio. Si no especificas ninguno, Heroku generará un nombre semialeatorio.

```
git push heroku master
```
Cuando enviemos este comando nos pedira un usuario y una contraseña.

Esto nos devuelve un token que nos servira para enviar nuestro proyecto a los servidores de Heroku mediante el comando git push.


<h5>¿Que es pushear?</h5>
Es subir los cambios hechos en tu entorno de trabajo local al repositorio remoto.

## Vamos a probar si todo funcionó

```
heroku open

```
Esto abrirá la aplicación en el navegador, en nuestro caso en:
```
https://drupal-demo-ba.herokuapp.com/core/install.php
```
De esta forma, podemos realizar la instalación de Drupal.
Completamos los pasos de instalación y felicidades, el sitio web de Drupal está listo.

## ¡Diste un gran paso!

¡Felicitaciones! Ya tienes tu sitio web online.
